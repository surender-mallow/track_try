module TrackTry
  class << self
    attr_accessor :configuration
  end

  # Configure TrackTry,
  # config/initializers/track_try.rb
  #
  # @example
  #  TrackTry.configure do |config|
  #    config.url = ''
  #    config.api_key = ''
  #  end

  def self.configuration
    @configuration ||= Configuration.new
  end

  def self.configure
    yield(configuration)
  end

  class Configuration
    attr_accessor :url, :api_key, :headers

    def url
      @url ||= "http://api.tracktry.com/v1"
    end

    def api_key
      @api_key ||= nil
    end

    def headers
      @headers ||= {
        'Content-Type': 'application/json',
        'Tracktry-Api-Key': @api_key.to_s
      }
    end
  end
end
