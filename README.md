# TrackTry

TrackTry gem to interact with TrackTry API in native Ruby

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'track_try'
```

And then execute:

    $ bundle install

Or install it yourself as:

    $ gem install track_try

## Configuration

```
# config/initializers/track_try.rb
TrackTry.configure do |config|
 config.url = ''
 config.api_key = ''
end
```

## Usage

```
# Get single tracking
response = TrackTry::Tracking.get(carrier_code, tracking_number)

# If success
response.id
response.status
....

# If error
It will raise TrackTry::Error with error message and status code.
```

## Contributing

Bug reports and pull requests are welcome on GitLab at https://gitlab.com/surender-mallow/track_try.

## License

The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).
