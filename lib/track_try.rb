# frozen_string_literal: true

require_relative "track_try/version"
require_relative "track_try/configuration"
require_relative "track_try/errors"
require_relative "track_try/tracker"
require_relative "track_try/tracking"

module TrackTry
  # Get single tracking
  def self.get_tracking!(carrier_code, tracking_number)
    if carrier_code.to_s.empty? || tracking_number.to_s.empty?
      raise TrackTry::Error.new('Carrier or tracking code does not exist', 404)
    else
      base_url = TrackTry.configuration.url
      headers = TrackTry.configuration.headers
      tracker('GET', "#{base_url}/trackings/#{carrier_code}/#{tracking_number}", headers)
    end
  end
end
