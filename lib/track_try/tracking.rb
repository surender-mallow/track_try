require 'json'

module TrackTry
  module Tracking
    def self.get(carrier_code, tracking_number)
      JSON.parse(TrackTry.get_tracking!(carrier_code, tracking_number).body, object_class: OpenStruct).data.first
    end
  end
end
