require 'uri'
require 'net/http'
require 'net/https'
require 'json'

module TrackTry
  def self.tracker(method, url, headers, data = nil)
    uri = URI.parse(url)
    if method == 'GET'
      @req = Net::HTTP::Get.new(uri.path, headers)
    elsif method == 'POST'
      @req = Net::HTTP::Post.new(uri.path, headers)
    end
    @req.body = data.to_json unless data.nil?

    https = Net::HTTP.new(uri.host, uri.port)
    https.use_ssl = true
    response = https.request(@req)
    if [200, 201].include?(response.code.to_i)
      response
    else
      raise TrackTry::Error.new(response.message, response.code)
    end
  end
end
